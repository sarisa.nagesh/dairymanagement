const express = require('express');
// const multer = require('multer');

const router = express.Router();
// const formDataHandler = multer();



const userRegister = require('../controllers/userRegistration');
const userLogin = require('../controllers/userLogin');
const animalRegister = require('../controllers/addAnimalController');
const updateAnimalDetails = require('../controllers/animalUpdate');
const Router = require('../controllers/file');
const deleteAnimalDetails = require('../controllers/removeAnimal');
const inseminationRegister = require('../controllers/addInseminationController');
const deliveryRegister = require('../controllers/addDeliveryController');
const repeatDateCheck = require('../controllers/getRepeatDateCheck');



router.post('/userRegister', userRegister);
router.post('/userLogin', userLogin);
router.post('/animalRegister', animalRegister);
router.post('/animalUpdate', updateAnimalDetails);
router.post('/UploadFile', Router);
router.post('/animalRemove', deleteAnimalDetails);
router.post('/addInsemination', inseminationRegister);
router.post('/addDelivery', deliveryRegister);
router.post('/getRepeatDateCheck', repeatDateCheck);

module.exports = router;
