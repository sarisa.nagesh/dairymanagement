const pool = require('../config/db');

function getInsemination (inseminationDetails) {
    const query = {
        name: 'Check insemination details exists',
        text: 'SELECT * FROM "public".insemination_data WHERE animal_id = $1',
        values: [ inseminationDetails.animal_id ]
    };

    try {
        return pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

function updateRepeatDate (inseminationDetails) {
    // console.log(inseminationDetails);
    const query = {
        name: 'Check insemination details update exists',
        text: `UPDATE "public".insemination_data SET repeat_date_check=$2 WHERE animal_id = $1`,
        values: [ inseminationDetails.animal_id, inseminationDetails.repeat_date_check, ],
    };

    try {
        return pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}


async function insertInsemination (inseminationDetails) {
    const query = {
        name: 'insert user in db',
        text: `INSERT INTO "public".insemination_data (insemination_date, animal_id, semination_type, inseminated_by, pregnancy_status, repeat_count ) 
        VALUES($1, $2, $3, $4, $5, $6 ) RETURNING *`,
        values: [
            inseminationDetails.insemination_date,
            inseminationDetails.animal_id,
            inseminationDetails.semination_type, 
            inseminationDetails.inseminated_by, 
            inseminationDetails.pregnancy_status, 
            inseminationDetails.repeat_count
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

module.exports = { getInsemination, insertInsemination, updateRepeatDate };
