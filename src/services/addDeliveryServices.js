const pool = require('../config/db');



async function insertDelivery (deliveryDetails) {
    const query = {
        name: 'insert user in db',
        text: `INSERT INTO "public".delivery_data (animal_id, date, calf_gender ) 
        VALUES($1, $2, $3 ) RETURNING *`,
        values: [
            deliveryDetails.animal_id,
            deliveryDetails.date,
            deliveryDetails.calf_gender
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

module.exports = { insertDelivery };
