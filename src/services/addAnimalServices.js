const pool = require('../config/db');


function updateImagePath(path,animal_id){
  //  UPDATE COMPANY SET SALARY = 15000 WHERE ID = 3;
    const query= {
         name: 'insert image path in db',
    text: `UPDATE  "public".animal_data SET  image_path=$2 WHERE animal_id=$1
          `,
    values: [
        animal_id,
        path,
    ]
}
try {
    return  pool.query(query);

    // console.log(`query result is ${result}`);

    // return result.rows.length;
} catch (error) {
    console.log(error);

    return error;
}
}

async function getAnimal (tag_no) {
    const query = {
        name: 'Check animal exists',
        text: 'SELECT * FROM "public".animal_data WHERE tag_no = $1',
        values: [ tag_no ],
    };

    

    try {
        return await pool.query(query);

        // console.log(`query result is ${result}`);

        // return result.rows.length;
    } catch (error) {
        console.log(error);

        return error;
    }
}

async function insertAnimal (animalDetails) {
    const query = {
        name: 'insert user in db',
        text: `INSERT INTO "public".animal_data (animal_type, tag_no, age, dob, status, lactation_no, breed_type, gender, last_pregnant_date, expected_delivery_date, current_calf_id, last_heat_date, last_modified_by, last_inserted_date, last_modified_date) 
                VALUES($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15) RETURNING *`,
        values: [
            animalDetails.animal_type,
            animalDetails.tag_no,
            animalDetails.age,
            animalDetails.dob,
            animalDetails.status,
            animalDetails.lactation_no,
            animalDetails.breed_type,
            animalDetails.gender,
            animalDetails.last_pregnant_date,
            animalDetails.expected_delivery_date,
            animalDetails.current_calf_id,
            animalDetails.last_heat_date,
            animalDetails.last_modified_by,
            animalDetails.last_inserted_date,
            animalDetails.last_modified_date,
            
           
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

async function updateAnimal (animalDetails) {
    const query = {
        name: 'update user in db',
        text: `UPDATE  "public".animal_data SET  animal_type=$2, breed_type=$3 WHERE animal_id=$1`,
        values: [
            animalDetails.animal_id,
            animalDetails.animal_type,
            animalDetails.breed_type,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}

async function removeAnimal (animalDetails) {
    const query = {
        name: 'delete user in db',
        text: `DELETE FROM "public".animal_data WHERE animal_id=$1`,
        values: [
            animalDetails.animal_id,
        ],
    };

    try {
        return await pool.query(query);
    } catch (error) {
        console.log(error);

        return error;
    }
}



module.exports = { getAnimal, insertAnimal , updateImagePath, updateAnimal, removeAnimal};
