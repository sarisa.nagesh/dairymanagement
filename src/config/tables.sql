/* now lets create all tables */


CREATE TABLE "public".user_info
(
    user_id SERIAL,
    user_name character varying(50) NOT NULL,
    mobile_num numeric(12,0) NOT NULL,
    email_id character varying,
    password character varying,
    CONSTRAINT user_pkey PRIMARY KEY (user_id),
    CONSTRAINT user_email_key UNIQUE (email_id),
    CONSTRAINT user_user_name_key UNIQUE (user_name),
    CONSTRAINT user_mobile_num_key UNIQUE (mobile_num)
);


CREATE TABLE "public".animal_data
(
    animal_id SERIAL,
    animal_type varchar(50) NULL,
    tag_no varchar(20)  NOT NULL,
    age int4 NULL,
    dob date NULL,
    status varchar(20) NULL,
    lactation_no int4 NULL,
    breed_type varchar(20) NULL,
    gender varchar(20) NULL,
    image_path varchar(100) NULL,
    last_pregnant_date date NULL,
    expected_delivery_date date NULL,
    current_calf_id int4 NULL,
    last_heat_date date NULL,
    last_modified_by varchar(100) NULL,
    last_modified_date date NULL,
    last_inserted_date date NULL,

    CONSTRAINT animal_pkey PRIMARY KEY (animal_id),
    CONSTRAINT animal_tag_no_key UNIQUE (tag_no)
);

CREATE TABLE "public".feed_data (
	feedname varchar(50) NULL,
	feed_id integer NULL,
	feedtype varchar(50) NULL,
	feedcostperkg numeric NULL,
	quantity_received numeric NULL,
	received_date date NULL,

    CONSTRAINT feed_pkey PRIMARY KEY (feed_id)
);

CREATE TABLE "public".produce_data (
	Date date NULL,
	Session_Type(AM/PM) varchar(2) NULL,
	produce_quantity float4 NULL,
	lactoreading float4 NULL,
	snf float4 NULL,
	fatpct float4 NULL,

    CONSTRAINT animal_id FOREIGN KEY (animal_id)
        REFERENCES "public".animal_data (animal_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
);

CREATE TABLE "public".insemination_data (
	insemination_id SERIAL,
    animal_id int4 NULL,
	insemination_date date NULL,
	semination_type varchar(20) NULL,
	inseminated_by varchar(30) NULL,
	pregnancy_status varchar(20) NULL,
	repeat_count varchar(20) NULL,
    repeat_date_check date NULL,

    CONSTRAINT animal_id FOREIGN KEY (animal_id)
        REFERENCES "public".animal_data (animal_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
);

CREATE TABLE "public".notifications (
	notification_id int4 NULL,
	notification_text text NULL,
	notification_status varchar(20) NULL,
	last_update_ts varchar(20) NULL
);

CREATE TABLE "public".medication_table (
	medicine_id int4 NULL,
	medicine_type varchar(50) NULL,
	medicine_cost numeric NULL,
	purpose_description text NULL
);

CREATE TABLE "public".worker_data (
	employee_id int4 NULL,
	employee_name varchar(50) NULL,
	aadhar_no numeric NULL,
	date_of_birth date NULL,
	date_of_joining date NULL,
	skills varchar(50) NULL,
	account_details varchar(500) NULL,
	salary float4 NULL,
	salary_last_paid_date date NULL,

    CONSTRAINT employee_pkey PRIMARY KEY (employee_id)
);

CREATE TABLE "public".delivery_data (
	delivery_id SERIAL,
    animal_id int4 NULL,
	date date NULL,
	calf_gender varchar(6) NULL,

    CONSTRAINT animal_id FOREIGN KEY (animal_id)
        REFERENCES "public".animal_data (animal_id) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE NO ACTION
);

