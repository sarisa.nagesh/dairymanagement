const winston = require('winston');

const { insertInsemination } = require('../services/addInseminationServices');
const { getAnimal } = require('../services/addAnimalServices');
const parseIp = require('../middleware/parseIp');


const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/Insemination.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const inseminationRegister = async (req, res) => {
    

    // using logger to record activity
    logger.info(`received insemination register request with ID: ${req.body.insemination_id}`);

    // using jwt let's get  the animal_id

    const { animal_id } = req.body;

    const animalDetails = await getAnimal(animal_id);

    if (animalDetails.rowCount != 0) {
        return res.json({
            statusCode: 404,
            message: 'Animal with that ID is not found',
        });
    }

    // after extracting the animal_id lets call the insert query
    else {
        const inseminationDetails = {
            insemination_date: req.body.insemination_date,
            animal_id,
            semination_type: req.body.semination_type,
            inseminated_by: req.body.inseminated_by,
            pregnancy_status: req.body.pregnancy_status,
            repeat_count: req.body.repeat_count,
        };
    
        const addInsemination = await insertInsemination(inseminationDetails);
    
        // if the insert function failed the it would return a false
        if (addInsemination.command === 'INSERT') {
            return res.json({ statusCode: 200,
                message: 'package registered!',
                details: addInsemination.rows,
                ipAddress: parseIp(req)
                 });
        }
    
        return res.json({ statusCode: 400,
            message: addInsemination.detail,
            ipAddress: parseIp(req) });
    }
    


    
};

module.exports = inseminationRegister;

