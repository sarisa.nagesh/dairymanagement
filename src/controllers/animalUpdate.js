const { updateAnimal } = require('../services/addAnimalServices');
const parseIp = require('../middleware/parseIp');



const  updateAnimalDetails = async (req, res) => {
    
  // using logger to record activity
  // logger.info(`received register request from manager with details ${req.body.animal_id}`);


  // storing all the animal data in one object to use it as a parameter
    const animalDetails = {
        animal_id: req.body.animal_id,
        animal_type: req.body.animal_type,
        breed_type: req.body.breed_type,

    };

const addAnimal = await updateAnimal(animalDetails);

    // if the insert function failed the it would return a false
    if (addAnimal.command === 'UPDATE') {
        // logger.info(`Animal UPDATED successfully`);

        return res.status(201)
            .json({ statusCode: 200,
                message: 'Sucess!',
                details: addAnimal.rows,
                ipAddress: parseIp(req) });
    }

    return res.status(400)
        .json({ statusCode: 400,
            message: addAnimal.detail,
            ipAddress: parseIp(req) });


    // const token = jwtManager(newUser.rows[0].animal_id);
};

module.exports = updateAnimalDetails;
