const { removeAnimal } = require('../services/addAnimalServices');
const parseIp = require('../middleware/parseIp');



const  deleteAnimalDetails = async (req, res) => {
    
  // using logger to record activity
  // logger.info(`received register request from manager with details ${req.body.animal_id}`);


  // storing all the animal data in one object to use it as a parameter
  const animalDetails = {
      animal_id: req.body.animal_id,
      
  };

const deleteAnimal = await removeAnimal(animalDetails);

    // if the insert function failed the it would return a false
    if (deleteAnimal.command === 'DELETE') {
        // logger.info(`Animal UPDATED successfully`);

        return res.status(201)
            .json({ statusCode: 200,
                message: 'Sucess!',
                details: deleteAnimal.rows,
                ipAddress: parseIp(req) });
    }

    return res.status(400)
        .json({ statusCode: 400,
            message: deleteAnimal.detail,
            ipAddress: parseIp(req) });


    // const token = jwtManager(newUser.rows[0].animal_id);
};

module.exports = deleteAnimalDetails;
