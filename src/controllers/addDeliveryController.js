const winston = require('winston');

const { insertDelivery } = require('../services/addDeliveryServices');
const { getAnimal } = require('../services/addAnimalServices');
const parseIp = require('../middleware/parseIp');


const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/Delivery.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const deliveryRegister = async (req, res) => {
    

    // using logger to record activity
    logger.info(`received insemination register request with ID: ${req.body.delivery_id}`);

    // using jwt let's get  the animal_id

    const { animal_id } = req.body;

    const animalDetails = await getAnimal(animal_id);

    if (animalDetails.rowCount != 0) {
        return res.json({
            statusCode: 404,
            message: 'Animal with that ID is not found',
        });
    }

    // after extracting the animal_id lets call the insert query
    else {
        const deliveryDetails = {
            animal_id,
            date: req.body.date,
            calf_gender: req.body.calf_gender
        };
    
        const addDelivery = await insertDelivery(deliveryDetails);
    
        // if the insert function failed the it would return a false
        if (addDelivery.command === 'INSERT') {
            return res.json({ statusCode: 200,
                message: 'package registered!',
                details: addDelivery.rows,
                ipAddress: parseIp(req)
                 });
        }
    
        return res.json({ statusCode: 400,
            message: addDelivery.detail,
            ipAddress: parseIp(req) });
    }
    


    
};

module.exports = deliveryRegister;

