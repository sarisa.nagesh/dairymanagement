const { getInsemination, updateRepeatDate } = require('../services/addInseminationServices');
const parseIp = require('../middleware/parseIp');


const  repeatDateCheck = async (req, res) => {
    
    const inseminationDetails = {
        animal_id: req.body.animal_id
    };
    
  
    const getInseminationDetails = await getInsemination(inseminationDetails);

        // console.log(getInseminationDetails.rowCount, 'is the insemination row count');

        // console.log(getInseminationDetails.rows);

        if (getInseminationDetails.rowCount === 0) {
            return res.json({
                statusCode: 404,
                message: 'animal with that ID is not found',
            });
        }

        
        const inseminationDate = getInseminationDetails.rows[0].insemination_date;

        // console.log(inseminationDate);

        // const addedDate = inseminationDate.getDate()+20;
    //   var repeatDateCheck = insemination_date.getDate() + 20;
    var newdate = new Date(inseminationDate);
    // console.log(newdate);
    newdate.setDate(inseminationDate.getDate()+20);
    
        // console.log(newdate);
        const date = {'newdate':newdate};


        const inseminationDetails1 = {
            animal_id: req.body.animal_id,
            repeat_date_check: date.newdate
        };

    const getRepeatDateCheckDetails = await updateRepeatDate(inseminationDetails1);

    
    
    if (getRepeatDateCheckDetails.command === 'UPDATE') {
        // logger.info(`Animal UPDATED successfully`);

        return res.status(200)
            .json({ statusCode: 200,
                message: 'Sucess!',
                details: getRepeatDateCheckDetails.rows,
                ipAddress: parseIp(req) });
    }

    return res.status(400)
        .json({ statusCode: 400,
            message: getRepeatDateCheckDetails.detail,
            ipAddress: parseIp(req) });
    
        
  };

module.exports = repeatDateCheck;

