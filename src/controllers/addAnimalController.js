const winston = require('winston');
const jwt = require('jsonwebtoken');

const { insertAnimal } = require('../services/addAnimalServices');
const parseIp = require('../middleware/parseIp');



const logger = winston.createLogger({
    transports: [
        new winston.transports.File({
            level: 'info',
            filename: 'logs/animal.log',
            json: true,
            format: winston.format.combine(winston.format.timestamp(),
                winston.format.json()),
        }),
    ],
});

const animalRegister = async (req, res) => {
    
    // using logger to record activity
    logger.info(`received register request from manager with details ${req.body.animal_id}`);


    // storing all the animal data in one object to use it as a parameter
    const animalDetails = {
        animal_type: req.body.animal_type,
        tag_no: req.body.tag_no,
        age: req.body.age,
        dob: req.body.dob,
        status: req.body.status,
        lactation_no: req.body.lactation_no,
        breed_type: req.body.breed_type,
        gender: req.body.gender,
        last_pregnant_date: req.body.last_pregnant_date,
        expected_delivery_date: req.body.expected_delivery_date,
        current_calf_id: req.body.current_calf_id,
        last_heat_date: req.body.last_heat_date,
        last_modified_by: req.body.last_modified_by,
        last_inserted_date: req.body.last_inserted_date,
        last_modified_date: req.body.last_modified_date,
       
        
    };
    const secret = '!@#DWe$%^gge&&**';
    const token = jwt.sign({ sub: req.body.animal_id }, secret, {
        expiresIn: 86400, // expires in 24 hours
    });

    const addAnimal = await insertAnimal(animalDetails);

    // if the insert function failed the it would return a false
    if (addAnimal.command === 'INSERT') {
        logger.info(`Animal added successfully`);

        return res.status(201)
            .json({ statusCode: 200,
                message: 'Sucess!',
                details: [addAnimal.rows,
                    {token: token, 
                    ipAddress: parseIp(req)}, ] });
    }

    return res.status(400)
        .json({ statusCode: 400,
            message: addAnimal.detail,
            ipAddress: parseIp(req) });


   
};

module.exports = animalRegister;
